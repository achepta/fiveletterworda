import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SolverManager {

    private List<String> words;
    private final int size;
    private int[] intWords;

    public SolverManager(String filename) {
        try {
            words = Files.readAllLines(Path.of(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        size = words.size();
        convertWordsToInts();
    }

    private void convertWordsToInts() {
        intWords = new int[size];
        for (int i = 0; i < size; i++) {
            intWords[i] = convertWordToInt(words.get(i));
        }
    }

    private int convertWordToInt(String word) {
        int intWord = 0;
        for (int i = 0; i < word.length(); i++) {
            intWord += 1 << (word.codePointAt(i)-97);
        }
        return intWord;
    }

    public void solve(int threadsAmount) {
        Solver[] solvers = getSolvers(threadsAmount);
        Thread[] threads = new Thread[threadsAmount];
        for (int i = 0; i < threadsAmount; i++) {
            threads[i] = new Thread(solvers[i]);
        }
        for (int i = 0; i < threadsAmount; i++) {
            threads[i].start();
        }
        for (int i = 0; i < threadsAmount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Solver[] getSolvers(int amount) {
        Solver[] solvers = new Solver[amount];
        for (int i = 0; i < amount; i++) {
            solvers[i] = getSolver(i, amount);
        }
        return solvers;
    }

    private Solver getSolver(int order, int total) {
        return new Solver(order, total, size, intWords, words);
    }
}
