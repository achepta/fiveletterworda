public class Cracker {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        new SolverManager("resources/words_alpha.txt").solve(1);
        System.out.println("Elapsed time: " + (System.currentTimeMillis()-start));
    }
}
