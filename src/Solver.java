import java.util.List;

public record Solver(int order, int total, int size, int[] intWords, List<String> words) implements Runnable {

    @Override
    public void run() {
        solve();
    }

    public void solve() {
        for (int i = order; i < size; i += total) {
            for (int j = i + 1; j < size; j++) {
                if ((intWords[i] & intWords[j]) == 0) {
                    for (int k = j + 1; k < size; k++) {
                        if ((intWords[i] & intWords[k]) == (intWords[j] & intWords[k])) {
                            for (int l = k + 1; l < size; l++) {
                                if ((intWords[i] & intWords[l]) == 0 && (intWords[j] & intWords[l]) == 0 && (intWords[k] & intWords[l]) == 0) {
                                    for (int m = l + 1; m < size; m++) {
                                        if ((intWords[i] & intWords[m]) == 0 && (intWords[j] & intWords[m]) == 0 && (intWords[k] & intWords[m]) == 0 && (intWords[l] & intWords[m]) == 0) {
                                            System.out.println(words.get(i) + " " + words.get(j) + " " + words.get(k) + " " + words.get(l) + " " + words.get(m));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
